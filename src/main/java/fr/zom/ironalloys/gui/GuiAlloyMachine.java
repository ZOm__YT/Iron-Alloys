package fr.zom.ironalloys.gui;

import fr.zom.ironalloys.container.ContainerAlloyMachine;
import fr.zom.ironalloys.tileentity.TileEntityAlloyMachine;
import fr.zom.ironalloys.utils.Refs;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiAlloyMachine extends GuiContainer
{

	private static final ResourceLocation background = new ResourceLocation(Refs.MODID, "textures/gui/container/alloy_machine.png");
	private TileEntityAlloyMachine tile;

	public GuiAlloyMachine(TileEntityAlloyMachine tile, InventoryPlayer playerInv)
	{
		super(new ContainerAlloyMachine(tile, playerInv));
		this.tile = tile;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		// Lol
		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;
		this.drawDefaultBackground();
		this.mc.getTextureManager().bindTexture(background);
		this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);

		int timePassed = this.tile.getField(1);
		int textureWidth = (int) (23f / 140f * timePassed);
		this.drawTexturedModalRect(i + 80, j + 35, 177, 14, textureWidth, 14);

		if( this.tile.isBurning() )
		{
			int burningTime = this.tile.getField(0);
			int textureHeight = (int) (12f / this.tile.getFullBurnTime() * burningTime);
			this.drawTexturedModalRect(i + 56, j + 26 + 25 - textureHeight,
									   175, 12 - textureHeight, 31, textureHeight);
		}

		this.fontRenderer.drawString(this.tile.getName(), i + 77, j + 60, 0xFFFFFF);
	}
}
