package fr.zom.ironalloys.utils;

public class Refs
{

	public static final String MODID = "ironalloys";
	public static final String NAME = "Z'Iron Alloys";
	public static final String VERSION = "1.0";

	public static final String CP = "fr.zom.ironalloys.proxy.ClientProxy";
	public static final String SP = "fr.zom.ironalloys.proxy.ServerProxy";
}
