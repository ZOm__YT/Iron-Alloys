/*
 * Crée et codé par Zom'
 */

package fr.zom.ironalloys.utils;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class EntityRenderManager extends RenderLiving
{
	private ResourceLocation texture;

	public EntityRenderManager(RenderManager rendermanagerIn, ModelBase modelBase, float shadowSize, ResourceLocation resourceLocation)
	{
		super(rendermanagerIn, modelBase, shadowSize);
		texture = resourceLocation;
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		return getTexture((EntityLiving) entity);
	}

	protected ResourceLocation getTexture(EntityLiving entityLiving)
	{
		return texture;
	}

}
