package fr.zom.ironalloys;

import fr.zom.ironalloys.ct.IATab;
import fr.zom.ironalloys.events.RegisteringEvent;
import fr.zom.ironalloys.proxy.CommonProxy;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

import static fr.zom.ironalloys.utils.Refs.*;

@Mod(modid = MODID, name = NAME, version = VERSION)
public class IronAlloys
{

	public static final CreativeTabs modTab = new IATab("ironAlloysTab");

	@Mod.Instance(MODID)
	public static IronAlloys instance;

	@SidedProxy(clientSide = CP, serverSide = SP)
	public static CommonProxy proxy;

	public IronAlloys()
	{
		MinecraftForge.EVENT_BUS.register(new RegisteringEvent());
	}

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		proxy.preInit();
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent e)
	{
		proxy.init();
	}

	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent e)
	{
		proxy.postInit();
	}

}
