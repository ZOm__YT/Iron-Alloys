/*
 * Crée et codé par Zom'
 */

package fr.zom.ironalloys.init;

import fr.zom.ironalloys.IronAlloys;
import fr.zom.ironalloys.entity.classes.EntityElemental;
import fr.zom.ironalloys.utils.EntityRenderManager;
import fr.zom.ironalloys.utils.Refs;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBlaze;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.awt.*;

public class ModEntities
{
	public static final ModEntities instance = new ModEntities();
	private int entityID = 0;

	public void registerEntities()
	{

			registerEntity(EntityElemental.class, "elemental");

	}

	private void registerEntity(Class<? extends Entity> entityClass, String entityName)
	{

		ResourceLocation res = new ResourceLocation(Refs.MODID, entityName);

		EntityRegistry.registerModEntity(res, entityClass, entityName, entityID++, IronAlloys.instance, 60, 1, true);
		EntityRegistry.registerEgg(res, new Color(0x76D6CC).getRGB(), new Color(0x9FCED7).getRGB());

	}

	@SideOnly(Side.CLIENT)
	public void registerEntityRenders()
	{

		registerEntityRender(EntityElemental.class, new ModelBlaze(), 0.6f, "elemental");

	}

	private void registerEntityRender(Class<? extends Entity> entity, ModelBase model, float shadowSize, String name)
	{
			RenderingRegistry.registerEntityRenderingHandler(entity, manager -> new EntityRenderManager(manager, model, shadowSize, new ResourceLocation(Refs.MODID, name)));
	}
}
