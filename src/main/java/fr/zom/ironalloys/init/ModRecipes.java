package fr.zom.ironalloys.init;

import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModRecipes
{

	public static ModRecipes instance = new ModRecipes();

	public void initRecipes()
	{

		GameRegistry.addSmelting(Blocks.IRON_BLOCK, new ItemStack(ModItems.ironBase, 4), 2.5f);

	}
}
