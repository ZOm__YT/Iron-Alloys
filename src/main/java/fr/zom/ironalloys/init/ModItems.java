package fr.zom.ironalloys.init;

import fr.zom.ironalloys.items.BasicItem;
import fr.zom.ironalloys.items.ItemHammer;
import fr.zom.ironalloys.items.ItemLightningStaff;
import fr.zom.ironalloys.utils.Refs;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.ArrayList;
import java.util.List;

public class ModItems
{

	public static final ModItems instance = new ModItems();

	public static Item ironBase, electric_iron, energized_iron;
	public static Item hammer;
	public static Item lightning_essence;
	public static Item lightning_staff;

	private List<Item> items = new ArrayList<>();

	public void init()
	{

		ironBase = new BasicItem("ironBase");
		electric_iron = new BasicItem("electric_iron");
		energized_iron = new BasicItem("energized_iron");

		lightning_essence = new BasicItem("lightning_essence");
		lightning_staff = new ItemLightningStaff("lightning_staff");

		hammer = new ItemHammer("hammer");

	}

	@SubscribeEvent
	public void registerRenders(ModelRegistryEvent e)
	{
		for ( Item item : items )
		{
			registerRender(item);
		}
	}

	public void registerRender(Item item)
	{
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(new ResourceLocation(Refs.MODID, item.getUnlocalizedName().substring(5)), "inventory"));

	}

	public List<Item> getItems()
	{
		return items;
	}
}
