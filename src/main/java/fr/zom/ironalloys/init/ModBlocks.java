package fr.zom.ironalloys.init;

import fr.zom.ironalloys.blocks.BlockAlloyMachine;
import fr.zom.ironalloys.utils.Refs;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModBlocks
{

	public static final ModBlocks instance = new ModBlocks();

	public static final Block alloyMachine = new BlockAlloyMachine("alloy_machine", Material.IRON);

	private Block[] blocks;

	public void init()
	{

		blocks = new Block[]{alloyMachine};

		for ( Block b : blocks )
		{
			ItemBlock ib = new ItemBlock(b);
			ib.setRegistryName(b.getRegistryName());
			GameRegistry.findRegistry(Item.class).register(ib);
		}

	}

	@SubscribeEvent
	public void registerModels(ModelRegistryEvent e)
	{

		for ( Block block : blocks )
		{
			registerModel(block);
		}

	}

	void registerModel(Block block)
	{

		ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(block), 0, new ModelResourceLocation(new ResourceLocation(Refs.MODID, block.getUnlocalizedName().substring(5)), "inventory"));

	}

	public Block[] getBlocks()
	{
		return this.blocks;
	}
}
