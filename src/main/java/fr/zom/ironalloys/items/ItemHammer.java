package fr.zom.ironalloys.items;

import fr.zom.ironalloys.init.ModItems;
import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Random;

public class ItemHammer extends BasicItem
{

	public ItemHammer(String name)
	{
		super(name);
		this.setMaxStackSize(1);
		this.setMaxDamage(64);
	}

	@Override
	public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{

		if( !worldIn.isRemote )
		{

			Block block = worldIn.getBlockState(pos).getBlock();

			if( block == Blocks.IRON_BLOCK )
			{

				worldIn.destroyBlock(pos, false);

				EntityItem loot = new EntityItem(worldIn);
				loot.setPosition(pos.getX(), pos.getY() + 0.6d, pos.getZ());
				loot.setItem(new ItemStack(ModItems.ironBase, 4));

				worldIn.spawnEntity(loot);

				player.getHeldItem(EnumHand.MAIN_HAND).damageItem(1, player);

			}

		}
		else
		{

			int r = new Random().nextInt(99) + 1;

			if( r > 95 )
			{

				worldIn.playSound(player, player.getPosition(), SoundEvents.BLOCK_ANVIL_USE, SoundCategory.BLOCKS, 1.f, 1.f);

			}

		}


		return super.onItemUse(player, worldIn, pos, hand, facing, hitX, hitY, hitZ);
	}
}
