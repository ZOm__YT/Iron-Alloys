package fr.zom.ironalloys.items;

import fr.zom.ironalloys.IronAlloys;
import fr.zom.ironalloys.init.ModItems;

public class BasicItem extends net.minecraft.item.Item
{

	public BasicItem(String name)
	{
		this.setRegistryName(name).setUnlocalizedName(name);
		this.setCreativeTab(IronAlloys.modTab);
		ModItems.instance.getItems().add(this);
	}
}
