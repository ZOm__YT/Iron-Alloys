/*
 * Crée et codé par Zom'
 */

package fr.zom.ironalloys.items;

import fr.zom.ironalloys.entity.classes.EntityElemental;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class ItemLightningStaff extends BasicItem
{
	public ItemLightningStaff(String name)
	{
		super(name);
		this.setMaxDamage(1);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn)
	{
		RayTraceResult rayCast = playerIn.rayTrace(10.D, 20);
		BlockPos rayPos = rayCast.getBlockPos();
		EntityLightningBolt bolt = new EntityLightningBolt(worldIn, rayPos.getX(), rayPos.getY(), rayPos.getZ(), true);

		if( !worldIn.isRemote )
		{
			if( rayCast.entityHit instanceof EntityElemental )
			{
				worldIn.removeEntity(rayCast.entityHit);
				worldIn.spawnEntity(bolt);
			}

		}
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}
}
