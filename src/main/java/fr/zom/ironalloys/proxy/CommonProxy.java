package fr.zom.ironalloys.proxy;

import fr.zom.ironalloys.IronAlloys;
import fr.zom.ironalloys.events.ZEntityEvents;
import fr.zom.ironalloys.handlers.IAGuiHandler;
import fr.zom.ironalloys.init.ModEntities;
import fr.zom.ironalloys.init.ModRecipes;
import fr.zom.ironalloys.tileentity.TileEntityAlloyMachine;
import fr.zom.ironalloys.utils.Refs;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class CommonProxy
{


	public void preInit()
	{


		// Enregisterement des tile entities

		GameRegistry.registerTileEntity(TileEntityAlloyMachine.class, Refs.MODID + ":alloy_machine_te");


		// Enregistrement du GuiHandler

		NetworkRegistry.INSTANCE.registerGuiHandler(IronAlloys.instance, new IAGuiHandler());

		ModEntities.instance.registerEntities();

	}


	public void init()
	{
		ModRecipes.instance.initRecipes();

		// Events Registering

		MinecraftForge.EVENT_BUS.register(new ZEntityEvents());

	}

	public void postInit()
	{

	}
}
