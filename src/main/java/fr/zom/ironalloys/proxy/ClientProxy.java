package fr.zom.ironalloys.proxy;

import fr.zom.ironalloys.init.ModBlocks;
import fr.zom.ironalloys.init.ModEntities;
import fr.zom.ironalloys.init.ModItems;
import net.minecraftforge.common.MinecraftForge;

public class ClientProxy extends CommonProxy
{

	@Override
	public void preInit()
	{
		super.preInit();

		MinecraftForge.EVENT_BUS.register(ModItems.instance);
		MinecraftForge.EVENT_BUS.register(ModBlocks.instance);

		ModEntities.instance.registerEntityRenders();

	}


	@Override
	public void init()
	{
		super.init();
	}

	@Override
	public void postInit()
	{
		super.postInit();
	}


}
