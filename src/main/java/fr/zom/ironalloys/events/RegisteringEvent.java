package fr.zom.ironalloys.events;

import fr.zom.ironalloys.init.ModBlocks;
import fr.zom.ironalloys.init.ModItems;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class RegisteringEvent
{

	@SubscribeEvent
	public void onItemRegister(RegistryEvent.Register<Item> e)
	{

		ModItems.instance.init();

		e.getRegistry().registerAll(ModItems.instance.getItems().toArray(new Item[0]));

	}

	@SubscribeEvent
	public void onBlockRegister(RegistryEvent.Register<Block> e)
	{
		ModBlocks.instance.init();
		e.getRegistry().registerAll(ModBlocks.instance.getBlocks());
	}

}
