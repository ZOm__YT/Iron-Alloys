/*
 * Crée et codé par Zom'
 */

package fr.zom.ironalloys.events;

import fr.zom.ironalloys.entity.classes.EntityElemental;
import fr.zom.ironalloys.init.ModItems;
import net.minecraft.block.BlockChest;
import net.minecraft.entity.Entity;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.EntityStruckByLightningEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ZEntityEvents
{

	/* Work in progress
	{@SubscribeEvent
	public void onLightningAppears(Entity e)
	{

		if( e.getEntity() instanceof EntityLightningBolt )
		{
			System.out.println("work");
			Entity ent = e.getEntity();

			BlockPos pos = ent.getPosition();

			if( !ent.world.isRemote )
			{
				ent.world.setBlockState(pos.up(2), Blocks.STONEBRICK.getDefaultState());
			}
			System.out.println(ent.getPosition());

			EntityItem loot = new EntityItem(ent.world);
			loot.setItem(new ItemStack(ModItems.lightning_essence, 16));

			loot.setPosition(pos.getX(), pos.getY() + 3.2D, pos.getZ());

			if( !ent.world.isRemote )
			{
				ent.world.spawnEntity(loot);
			}

		}
	})
	*/

	@SubscribeEvent
	public void onElementalStruckByLightning(EntityStruckByLightningEvent e)
	{
		Entity ent = e.getEntity();
		EntityLightningBolt bolt = e.getLightning();

		World world = ent.world;

		if(ent instanceof EntityElemental )
		{

			world.removeEntity(ent);
			world.removeEntity(bolt);

			BlockPos pos = ent.getPosition();

			EntityItem loot = new EntityItem(world);
			loot.setItem(new ItemStack(ModItems.lightning_essence, 16));
			loot.setPosition(pos.getX()+0.5d, pos.getY()+5.D, pos.getZ()+0.5d);

			if(!world.isRemote)
			{

				world.setBlockState(pos.up(2), Blocks.STONEBRICK.getDefaultState());
				world.spawnEntity(loot);


			}



		}
	}
}
