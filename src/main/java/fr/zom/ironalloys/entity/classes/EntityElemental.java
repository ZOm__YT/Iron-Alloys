/*
 * Crée et codé par Zom'
 */

package fr.zom.ironalloys.entity.classes;

import fr.zom.ironalloys.utils.Refs;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBlaze;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.awt.*;

public class EntityElemental extends BasicEntity
{

	public EntityElemental(World worldIn)
	{
		super(worldIn, 2.5D, 0.0D, 1, 1.8f, 0.6f);
	}

	@Override
	public String getName()
	{
		return "elemental_mob";
	}

	@Nullable
	@Override
	protected ResourceLocation getLootTable()
	{
		return new ResourceLocation(Refs.MODID, getName());
	}

	@Override
	protected boolean canDropLoot()
	{
		return true;
	}
}
