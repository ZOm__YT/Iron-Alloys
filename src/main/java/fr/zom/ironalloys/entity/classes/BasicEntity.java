/*
 * Crée et codé par Zom'
 */

package fr.zom.ironalloys.entity.classes;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.*;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;

public class BasicEntity extends EntityMob
{
	private static double maxHealthV;
	private static double attackDamageV;

	public BasicEntity(World worldIn, double maxHealthV, double attackDamageV, int xp, float height, float width)
	{
		super(worldIn);

		this.maxHealthV = maxHealthV * 2;
		this.attackDamageV = attackDamageV * 2;
		this.experienceValue = xp;
		this.setSize(width, height);

		tasks.addTask(2, new EntityAIAttackMelee(this, 0.9D, true));
		tasks.addTask(4, new EntityAIMoveTowardsRestriction(this, 0.9D));
		tasks.addTask(5, new EntityAIWander(this, 0.9D));
		tasks.addTask(6, new EntityAILookIdle(this));
		targetTasks.addTask(1, new EntityAIHurtByTarget(this, false));


	}


	@Override
	protected void applyEntityAttributes()
	{
		super.applyEntityAttributes();

		this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(maxHealthV);

		this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(attackDamageV);

		this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.3056125);

		this.getEntityAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(35);
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return SoundEvents.ENTITY_PLAYER_HURT;
	}

	@Override
	public boolean getCanSpawnHere()
	{
		return this.world.getDifficulty() != EnumDifficulty.PEACEFUL;
	}

}
