package fr.zom.ironalloys.blocks;

import fr.zom.ironalloys.IronAlloys;
import fr.zom.ironalloys.tileentity.TileEntityAlloyMachine;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class BlockAlloyMachine extends BlockContainer
{
	public BlockAlloyMachine(String name, Material materialIn)
	{
		super(materialIn);

		this.setRegistryName(name).setUnlocalizedName(name).setCreativeTab(IronAlloys.modTab);

		this.setHardness(10.f);
		this.setResistance(40.f);

		this.setHarvestLevel("pickaxe", 2);
	}

	@Override
	public boolean hasTileEntity(IBlockState state)
	{
		return true;
	}

	@Nullable
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileEntityAlloyMachine();
	}

	// Lorsque on casse le bloc on fait drop tout l'inventaire qu'il contient

	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
	{
		TileEntity te = worldIn.getTileEntity(pos);

		if( te instanceof TileEntityAlloyMachine )
		{

			InventoryHelper.dropInventoryItems(worldIn, pos, (TileEntityAlloyMachine) te);

		}
		super.breakBlock(worldIn, pos, state);
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{

		if( worldIn.isRemote )
		{
			return true;
		}
		else
		{

			TileEntity te = worldIn.getTileEntity(pos);

			if( te instanceof TileEntityAlloyMachine )
			{

				playerIn.openGui(IronAlloys.instance, 0, worldIn, pos.getX(), pos.getY(), pos.getZ());

			}

			return true;

		}

	}

	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.MODEL;
	}

	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
	{
		if( stack.hasDisplayName() )
		{
			TileEntity te = worldIn.getTileEntity(pos);

			if( te instanceof TileEntityAlloyMachine )
			{

				((TileEntityAlloyMachine) te).setCustomName(stack.getDisplayName());

			}
		}
	}
}
