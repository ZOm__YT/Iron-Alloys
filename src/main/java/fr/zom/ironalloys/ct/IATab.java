package fr.zom.ironalloys.ct;

import fr.zom.ironalloys.init.ModItems;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class IATab extends CreativeTabs
{
	public IATab(String label)
	{
		super(label);
	}

	@Override
	public ItemStack getTabIconItem()
	{
		return new ItemStack(ModItems.ironBase);
	}
}
