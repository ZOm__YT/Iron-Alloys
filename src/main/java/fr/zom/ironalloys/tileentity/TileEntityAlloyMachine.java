package fr.zom.ironalloys.tileentity;

import com.google.common.collect.Lists;
import fr.zom.ironalloys.init.ModItems;
import fr.zom.ironalloys.tileentity.recipes.RecipesAlloyMachine;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntityLockable;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.TextFormatting;

import java.util.List;

public class TileEntityAlloyMachine extends TileEntityLockable implements ITickable
{

	// Variables

	private NonNullList<ItemStack> stacks = NonNullList.withSize(4, ItemStack.EMPTY);
	private String customName;
	private int timePassed = 0;
	private int burningTimeLeft = 0;

	private List<Item> fuelList = Lists.newArrayList(Items.COAL, Items.LAVA_BUCKET);


	@Override
	public void readFromNBT(NBTTagCompound compound)
	{
		super.readFromNBT(compound);
		this.stacks = NonNullList.withSize(this.getSizeInventory(), ItemStack.EMPTY);
		ItemStackHelper.loadAllItems(compound, this.stacks);

		if( compound.hasKey("CustomName", 8) )
		{
			this.customName = compound.getString("CustomName");
		}

		this.burningTimeLeft = compound.getInteger("burningTimeLeft");
		this.timePassed = compound.getInteger("timePassed");
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound)
	{
		super.writeToNBT(compound);
		ItemStackHelper.saveAllItems(compound, this.stacks);

		if( this.hasCustomName() )
		{
			compound.setString("CustomName", this.customName);
		}

		compound.setInteger("burningTimeLeft", this.burningTimeLeft);
		compound.setInteger("timePassed", this.timePassed);

		return compound;

	}

	@Override
	public int getSizeInventory()
	{
		return this.stacks.size();
	}

	@Override
	public ItemStack getStackInSlot(int index)
	{
		return this.stacks.get(index);
	}

	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		return ItemStackHelper.getAndSplit(this.stacks, index, count);
	}

	@Override
	public ItemStack removeStackFromSlot(int index)
	{
		return ItemStackHelper.getAndRemove(this.stacks, index);
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		this.stacks.set(index, stack);

		if( stack.getCount() > this.getInventoryStackLimit() )
		{
			stack.setCount(this.getInventoryStackLimit());
		}
	}

	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}


	@SuppressWarnings("SimplifiableConditionalExpression")
	@Override
	public boolean isUsableByPlayer(EntityPlayer player)
	{
		return this.world.getTileEntity(this.pos) != this ? false : player.getDistanceSq((double) this.pos.getX() + 0.5d, (double) this.pos.getY() + 0.5d, (double) this.pos.getZ() + 0.5d) <= 64.0d;
	}

	@Override
	public void openInventory(EntityPlayer player)
	{

	}

	@Override
	public void closeInventory(EntityPlayer player)
	{

	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{

		if( index == 0 )
		{
			return stack.getItem() == ModItems.ironBase;
		}

		if( index == 2 )
		{
			return fuelList.contains(stack.getItem());
		}

		if( index == 3 )
		{
			return false;
		}

		return true;
	}

	@Override
	public int getField(int id)
	{
		switch ( id )
		{

			case 0:
				return this.burningTimeLeft;
			case 1:
				return this.timePassed;

		}

		return 0;
	}

	@Override
	public void setField(int id, int value)
	{
		switch ( id )
		{
			case 0:
				this.burningTimeLeft = value;
				break;
			case 1:
				this.timePassed = value;
		}
	}

	@Override
	public int getFieldCount()
	{
		return 2;
	}

	@Override
	public boolean isEmpty()
	{
		for ( ItemStack stack : this.stacks )
		{
			if( !stack.isEmpty() )
			{
				return false;
			}
		}
		return true;
	}

	@Override
	public void clear()
	{
		for ( int i = 0; i < this.stacks.size(); i++ )
		{
			this.stacks.set(i, ItemStack.EMPTY);
		}
	}

	public void setCustomName(String name)
	{
		this.customName = name;
	}

	@Override
	public String getName()
	{
		return hasCustomName() ? this.customName : I18n.format("alloyMachineTE", TextFormatting.RESET);
	}

	@Override
	public boolean hasCustomName()
	{
		return this.customName != null && !this.customName.isEmpty();
	}

	@Override
	public void update()
	{
		if( !this.world.isRemote )
		{

			/* Si le carburant brûle, on réduit réduit le temps restant */
			if( this.isBurning() )
			{
				this.burningTimeLeft--;
			}

			/*
			 * Si la on peut faire cuire la recette et que le four ne cuit pas
			 * alors qu'il peut, alors on le met en route
			 */
			if( !this.isBurning() && this.canSmelt() && !this.hasFuelEmpty() )
			{
				this.burningTimeLeft = this.getFullBurnTime();
				this.decrStackSize(2, 1);
			}

			/* Si on peut faire cuire la recette et que le feu cuit */
			if( this.isBurning() && this.canSmelt() )
			{
				this.timePassed++;
				if( timePassed >= this.getFullRecipeTime() )
				{
					timePassed = 0;
					this.smelt();
				}
			}
			else
			{
				timePassed = 0;
			}
			this.markDirty();
		}
	}

	@Override
	public Container createContainer(InventoryPlayer playerInventory, EntityPlayer playerIn)
	{
		return null;
	}

	@Override
	public String getGuiID()
	{
		return null;
	}

	public boolean hasFuelEmpty()
	{
		return this.getStackInSlot(2).isEmpty();
	}

	public ItemStack getRecipeResult()
	{
		return RecipesAlloyMachine.getRecipeResult(new ItemStack[]{this.getStackInSlot(0), this.getStackInSlot(1)});
	}

	public boolean canSmelt()
	{
		ItemStack result = this.getRecipeResult();

		if( result != null )
		{

			ItemStack slot4 = this.getStackInSlot(3);

			if( slot4.isEmpty() )
			{
				return true;
			}

			if( slot4.getItem() == result.getItem() && slot4.getItemDamage() == result.getItemDamage() )
			{
				int newStackSize = slot4.getCount() + result.getCount();
				if( newStackSize <= this.getInventoryStackLimit() && newStackSize <= slot4.getMaxStackSize() )
				{
					return true;
				}
			}
		}
		return false;
	}

	public void smelt()
	{
		// Cette fonction n'est appelée que si result != null, c'est pourquoi on ne fait pas de null check
		ItemStack result = this.getRecipeResult();
		// On enlève un item de chaque ingrédient
		this.decrStackSize(0, 1);
		this.decrStackSize(1, 1);
		// On récupère le slot de résultat
		ItemStack stack4 = this.getStackInSlot(3);
		// Si il est vide
		if( stack4.isEmpty() )
		{
			// On y insère une copie du résultat
			this.setInventorySlotContents(3, result.copy());
		}
		else
		{
			// Sinon on augmente le nombre d'objets de l'ItemStack
			stack4.setCount(stack4.getCount() + result.getCount());
		}
	}

	public int getFullRecipeTime()
	{
		return 140;
	}

	public int getFullBurnTime()
	{
		return 560;
	}

	public boolean isBurning()
	{
		return burningTimeLeft > 0;
	}
}
