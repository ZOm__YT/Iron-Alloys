package fr.zom.ironalloys.handlers;

import fr.zom.ironalloys.container.ContainerAlloyMachine;
import fr.zom.ironalloys.gui.GuiAlloyMachine;
import fr.zom.ironalloys.tileentity.TileEntityAlloyMachine;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

import javax.annotation.Nullable;

public class IAGuiHandler implements IGuiHandler
{
	@Nullable
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{

		TileEntity te = world.getTileEntity(new BlockPos(x, y, z));

		if( ID == 0 )
		{
			if( te instanceof TileEntityAlloyMachine )
			{
				return new ContainerAlloyMachine((TileEntityAlloyMachine) te, player.inventory);
			}
		}

		return null;
	}

	@Nullable
	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{

		TileEntity te = world.getTileEntity(new BlockPos(x, y, z));

		if( ID == 0 )
		{

			if( te instanceof TileEntityAlloyMachine )
			{
				return new GuiAlloyMachine((TileEntityAlloyMachine) te, player.inventory);
			}
		}

		return null;
	}
}
