package fr.zom.ironalloys.container;

import fr.zom.ironalloys.container.slots.SlotOutput;
import fr.zom.ironalloys.tileentity.TileEntityAlloyMachine;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnaceFuel;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ContainerAlloyMachine extends Container
{

	private TileEntityAlloyMachine tile;
	private int timePassed = 0;
	private int burnTimeLeft = 0;

	public ContainerAlloyMachine(TileEntityAlloyMachine tile, InventoryPlayer playerInventory)
	{
		this.tile = tile;

		int i;
		for ( i = 0; i < 2; i++ )
		{
			this.addSlotToContainer(new Slot(tile, i, 37 + i * 36, 17));
		}

		this.addSlotToContainer(new SlotFurnaceFuel(tile, 2, 56, 11 + i * 21));

		this.addSlotToContainer(new SlotOutput(tile, 3, 116, 35));

		for ( i = 0; i < 3; ++i )
		{
			for ( int j = 0; j < 9; ++j )
			{
				this.addSlotToContainer(new Slot(playerInventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
			}
		}

		for ( i = 0; i < 9; ++i )
		{
			this.addSlotToContainer(new Slot(playerInventory, i, 8 + i * 18, 142));
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn)
	{
		return tile.isUsableByPlayer(playerIn);
	}

	@Override
	public void addListener(IContainerListener listener)
	{
		super.addListener(listener);
		listener.sendAllWindowProperties(this, this.tile);
	}

	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();

		for ( int i = 0; i < this.listeners.size(); ++i )
		{
			IContainerListener icontainerlistener = (IContainerListener) this.listeners
					.get(i);

			if( this.burnTimeLeft != this.tile.getField(0) )
			{
				icontainerlistener.sendWindowProperty(this, 0,
													  this.tile.getField(0));
			}

			if( this.timePassed != this.tile.getField(1) )
			{
				icontainerlistener.sendWindowProperty(this, 1,
													  this.tile.getField(1));
			}
		}

		this.burnTimeLeft = this.tile.getField(0);
		this.timePassed = this.tile.getField(1);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int id, int data)
	{
		this.tile.setField(id, data);
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index)
	{
		return ItemStack.EMPTY;
	}
}
